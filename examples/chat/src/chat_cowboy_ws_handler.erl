-module(chat_cowboy_ws_handler).

-export([init/3]).
-export([websocket_init/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_terminate/3]).

-define(CHATROOM_NAME, ?MODULE).
-define(TIMEOUT, 5 * 60 * 1000). % Innactivity Timeout

-record(state, {name, handler}).

%% API

init(_, _Req, _Opts) ->
  {upgrade, protocol, cowboy_websocket}.

websocket_init(_Type, Req, _Opts) ->
  % Create the handler from our custom callback
  Handler = ebus_proc:spawn_handler(fun chat_erlbus_handler:handle_msg/2, [self()]),
  ebus:sub(Handler, ?CHATROOM_NAME),
  Name = get_name(Req),
  ebus:pub(?CHATROOM_NAME, {Name, list_to_binary(binary_to_list(Name) ++ " bergabung di obrolan")}),
  {ok, Req, #state{name = Name, handler = Handler}, ?TIMEOUT}.

websocket_handle({text, Msg}, Req, State) ->
  ebus:pub(?CHATROOM_NAME, {State#state.name, Msg}),
  {ok, Req, State};
websocket_handle(_Data, Req, State) ->
  {ok, Req, State}.

websocket_info({message_published, {Sender, Msg}}, Req, State) ->
  {reply, {text, jiffy:encode({[{sender, Sender}, {msg, Msg}]})}, Req, State};
websocket_info(_Info, Req, State) ->
  {ok, Req, State}.

websocket_terminate(_Reason, Req, State) ->
  % Unsubscribe the handler
  Name = get_name(Req),
  ebus:pub(?CHATROOM_NAME, {Name, list_to_binary(binary_to_list(Name) ++ " meninggalkan di obrolan")}),
  ebus:unsub(State#state.handler, ?CHATROOM_NAME),
  ok.

%% Private methods

get_name(Req) ->
  Data = element(1, cowboy_req:binding(username, Req)),
  io:fwrite("~p~n", [Data]),
  NamaAsli = binary_to_list(Data),
  {{Host, Port}, _} = cowboy_req:peer(Req),
  Name = list_to_binary(string:join([NamaAsli, " ", "(", inet_parse:ntoa(Host),
    ":", io_lib:format("~p", [Port]), ")"], "")),
  Name.
  